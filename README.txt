SFETCU IULIAN-ANDREI 322CD

Am creat urmatoarele clase:
	Carte
	Jucator
		Base ("basic strategy") extinde Jucator
		Greedy ("greedy strategy") extinde Jucator
		Bribe ("bribe strategy") extinde Jucator

Am ales ca jucatorii sa ii stochez intr-un vector, iar initializarea fiecaruia sa o fac folosind
constructorii pe care i-am creat in fiecare tip de strategie.
In fiecare din cele 3 clase strategie, am definit 3 metode:
	- takecards() ce ia carti din pachet si le pune in lista cartilor din mana
	- playnormal() ce joaca rolul de comerciant in functie de tipul jucatorului
	- playsheriff() ce joaca rolul de sheriff in functie de tipul jucatorului

Am ales sa adaug fiecarui jucator un camp sheriff de tip boolean pe care il voi seta pe true in
functie de numarul rundei. 
Metoda sheriff verifica comerciantii, accepta sau nu mita/ modifica banii player-ului verificat
si apoi pune cartile pe stand/ le distruge.

Bonusul pentru bunuri ilegale l-am implementat folosindu-ma de un camp suplimentar pentru Jucator 
si anume vectorul de evidenta al bunurilor legale de pe stand (player.products).
	0 - number of Apple
	1 - number of Cheese
	2 - number of Bread
	3 - number of Chicken

Bonusul pentru KING/QUEEN l-am implementat folosindu-ma de un vector de bonusuri kqbonuses.