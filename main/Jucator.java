package main;

import java.util.*;

public class Jucator{
	
	String type; // Strategy
	List<Carte> AssetsInHand;
	List<Carte> AssetsOnMerchantStand;
	int Coins;
	
	boolean sheriff; // True if player plays sheriff this round
	
	List<Carte> AssetsInSack;
	int bribe;
	String DeclaredType;
	
	int points;

	int[] products; // Evidence of legal products
	
	public Jucator(){
		
	}
	
	public void AddAssetsOnMerchantStand(Carte carte) {
		this.AssetsOnMerchantStand.add(carte);
	}
	

	public void playsheriff(Jucator player) {
		
	}
	
	public void playnormal(List<Integer> list, int integer) {
		
	}

	public void takecards(List<Integer> inputcards){

	}

    public void bubbleSort(Jucator[] array) {
        boolean swapped = true;
        int j = 0;
        Jucator tmp = new Jucator();
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < array.length - j; i++) {
                if (array[i].points < array[i + 1].points) {
                    tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                    swapped = true;
                }
            }
        }
    }

    public void getProducts(){

		this.products = new int[4];

		for(int j=0; j<this.products.length; j++)
			this.products[j] = 0;

		for(int i=0; i<this.AssetsOnMerchantStand.size(); i++) {
			if (this.AssetsOnMerchantStand.get(i).type.equals("Apple"))
				this.products[0]++;
			else if (this.AssetsOnMerchantStand.get(i).type.equals("Cheese"))
				this.products[1]++;
			else if (this.AssetsOnMerchantStand.get(i).type.equals("Bread"))
				this.products[2]++;
			else if (this.AssetsOnMerchantStand.get(i).type.equals("Chicken"))
				this.products[3]++;
		}
	}
}
