package main;

import java.util.*;
import java.util.List;

public class Base extends Jucator {

	public Base(String input) {
		this.type = input;
		this.Coins = 50;
		this.AssetsInHand = new LinkedList<>();
		this.AssetsOnMerchantStand = new LinkedList<>();
		this.AssetsInSack = new LinkedList<>();
		this.sheriff = false;
	}

	@Override
	public void takecards(List<Integer> pack) {

		int already = this.AssetsInHand.size();
		int count = 6 - already;

		// Pick remaining cards
		for (int i = 0; i < count; i++) {
			Carte carte = new Carte(pack.get(i));
			this.AssetsInHand.add(carte);
		}

		// Remove picked cards
		while (count > 0) {
			pack.remove(0);
			count--;
		}
	}

	@Override
	public void playnormal(List<Integer> pack, int roundNO) {

		int apple = 0;
		int cheese = 0;
		int bread = 0;
		int chicken = 0;
		int silk = 0;
		int pepper = 0;
		int barrel = 0;

		// Calculating all frequencies
		for(int i=0; i<this.AssetsInHand.size(); i++)
			if (this.AssetsInHand.get(i).type.equals("Apple"))
				apple++;
			else if (this.AssetsInHand.get(i).type.equals("Cheese"))
				cheese++;
			else if (this.AssetsInHand.get(i).type.equals("Bread"))
				bread++;
			else if (this.AssetsInHand.get(i).type.equals("Chicken"))
				chicken++;
			else if (this.AssetsInHand.get(i).type.equals("Silk"))
				silk++;
			else if (this.AssetsInHand.get(i).type.equals("Pepper"))
				pepper++;
			else if (this.AssetsInHand.get(i).type.equals("Barrel"))
				barrel++;

		// Setting frequency to every card
		for (int i = 0; i < this.AssetsInHand.size(); i++) {
			if (this.AssetsInHand.get(i).type.equals("Apple"))
				this.AssetsInHand.get(i).freq = apple;
			else if (this.AssetsInHand.get(i).type.equals("Cheese"))
				this.AssetsInHand.get(i).freq = cheese;
			else if (this.AssetsInHand.get(i).type.equals("Bread"))
				this.AssetsInHand.get(i).freq = bread;
			else if (this.AssetsInHand.get(i).type.equals("Chicken"))
				this.AssetsInHand.get(i).freq = chicken;
			else if (this.AssetsInHand.get(i).type.equals("Silk"))
				this.AssetsInHand.get(i).freq = silk;
			else if (this.AssetsInHand.get(i).type.equals("Pepper"))
				this.AssetsInHand.get(i).freq = pepper;
			else if (this.AssetsInHand.get(i).type.equals("Barrel"))
				this.AssetsInHand.get(i).freq = barrel;
		}

		// Calculating maximum frequencies
		int MAXlegal = Math.max(Math.max(apple, cheese), Math.max(bread, chicken));
		int MAXillegal = Math.max(Math.max(silk, pepper), barrel);

		int MAXprofitl = 0;
		int MAXprofitill = 0;

		int indexl = 5;
		int indexill = 5;

		Carte choice = new Carte();

		for (int i = 0; i < this.AssetsInHand.size(); i++) {

			if (this.AssetsInHand.get(i).freq == MAXlegal && this.AssetsInHand.get(i).profit <= 4) {
				if (this.AssetsInHand.get(i).profit > MAXprofitl) {
					MAXprofitl = this.AssetsInHand.get(i).profit;
					indexl = i;
				}
			}
			if (this.AssetsInHand.get(i).freq == MAXillegal && this.AssetsInHand.get(i).profit > 4) {
				if (this.AssetsInHand.get(i).profit > MAXprofitill) {
					MAXprofitill = this.AssetsInHand.get(i).profit;
					indexill = i;
				}
			}
		}

		for (int i = 0; i < this.AssetsInHand.size(); i++) {

			if (MAXlegal > 0) {
				if (this.AssetsInHand.get(i).freq == MAXlegal &&
						this.AssetsInHand.get(i).profit == MAXprofitl &&
						this.AssetsInHand.get(i).type.equals(this.AssetsInHand.get(indexl).type)) {

					this.AssetsInSack.add(this.AssetsInHand.get(i));
					choice = this.AssetsInHand.get(i);
				}
			} else {
				if (this.AssetsInHand.get(i).freq == MAXillegal &&
						this.AssetsInHand.get(i).profit == MAXprofitill &&
						this.AssetsInHand.get(i).type.equals(this.AssetsInHand.get(indexill).type)) {

					this.AssetsInSack.add(this.AssetsInHand.get(i));
					choice = this.AssetsInHand.get(i);
				}
			}
		}

		// Declaring type and bribe
		if (choice.profit > 4)
			this.DeclaredType = "Apple";
		else
			this.DeclaredType = choice.type;

		this.bribe = 0;

		// Delete choosen cards from hand
		for (int j = 0; j < this.AssetsInHand.size(); j++) {
			if (this.AssetsInHand.get(j).type.equals(choice.type)) {
				this.AssetsInHand.remove(j);
				j--;
			}
		}
	}

	@Override
	public void playsheriff (Jucator player){

			int take = 0;
			int give = 0;

			boolean honest = true;

			for (int i = 0; i < player.AssetsInSack.size(); i++) {

				if (player.AssetsInSack.get(i).type.equals(player.DeclaredType)) {
					give += player.AssetsInSack.get(i).penalty;
				}
				else {
					take += player.AssetsInSack.get(i).penalty;
					honest = false;
				}
			}

			if (honest == false)
				give = 0;

			for (int i = 0; i < player.AssetsInSack.size(); i++) {

				if (player.AssetsInSack.get(i).type.equals(player.DeclaredType))
					player.AddAssetsOnMerchantStand(player.AssetsInSack.get(i));

				player.AssetsInSack.remove(i);
				i--;
			}

			if(player.bribe != 0) {
				player.Coins += player.bribe;
				player.bribe = 0;
			}

			this.Coins -= give;
			player.Coins += give;
			this.Coins += take;
			player.Coins -= take;

	}

}
