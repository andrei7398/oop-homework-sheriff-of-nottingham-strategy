package main;

public class Carte {
	String type;
	int id;
	int profit;
	int penalty;
	int freq;

	// Default constructor
	Carte() {

	}

	// Constructor for a book with a given type
	Carte(String newtype) {
		
		this.type = newtype;
		
		if(newtype.equals("Apple")) {
			this.id = 0;
			this.profit = 2;
			this.penalty = 2;
		}
		else if(newtype.equals("Cheese")) {
			this.id = 1;
			this.profit = 3;
			this.penalty = 2;
		}
		else if(newtype.equals("Bread")) {
			this.id = 2;
			this.profit = 4;
			this.penalty = 2;
		}
		else if(newtype.equals("Chicken")) {
			this.id = 3;
			this.profit = 4;
			this.penalty = 2;
		}
		else if(newtype.equals("Silk")) {
			this.id = 10;
			this.profit = 9;
			this.penalty = 4;
		}
		else if(newtype.equals("Pepper")) {
			this.id = 11;
			this.profit = 8;
			this.penalty = 4;
		}
		else if(newtype.equals("Barrel")) {
			this.id = 12;
			this.profit = 7;
			this.penalty = 4;
		}
	}
	
	// Constructor for a book with a given id
	Carte(int newid) {
		
		this.id = newid;
		
		if(newid == 0) {
			this.type = "Apple";
			this.profit = 2;
			this.penalty = 2;
		}
		else if(newid == 1) {
			this.type = "Cheese";
			this.profit = 3;
			this.penalty = 2;
		}
		else if(newid == 2) {
			this.type = "Bread";
			this.profit = 4;
			this.penalty = 2;
		}
		else if(newid == 3) {
			this.type = "Chicken";
			this.profit = 4;
			this.penalty = 2;
		}
		else if(newid == 10) {
			this.type = "Silk";
			this.profit = 9;
			this.penalty = 4;
		}
		else if(newid == 11) {
			this.type = "Pepper";
			this.profit = 8;
			this.penalty = 4;
		}
		else if(newid == 12) {
			this.type = "Barrel";
			this.profit = 7;
			this.penalty = 4;
		}
	}

}
