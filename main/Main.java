// SFETCU IULIAN-ANDREI 322CD
package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public final class Main {

    private static final class GameInputLoader {
        private final String mInputPath;

        private GameInputLoader(final String path) {
            mInputPath = path;
        }

        public GameInput load() {
            List<Integer> assetsIds = new ArrayList<>();
            List<String> playerOrder = new ArrayList<>();

            try {
                BufferedReader inStream = new BufferedReader(new FileReader(mInputPath));
                String assetIdsLine = inStream.readLine().replaceAll("[\\[\\] ']", "");
                String playerOrderLine = inStream.readLine().replaceAll("[\\[\\] ']", "");

                for (String strAssetId : assetIdsLine.split(",")) {
                    assetsIds.add(Integer.parseInt(strAssetId));
                }

                for (String strPlayer : playerOrderLine.split(",")) {
                    playerOrder.add(strPlayer);
                }
                inStream.close();


            } catch (IOException e) {
                e.printStackTrace();
            }
            return new GameInput(assetsIds, playerOrder);
        }
    }

    public static void main(final String[] args) {
        GameInputLoader gameInputLoader = new GameInputLoader(args[0]);
        GameInput gameInput = gameInputLoader.load();

        List<Integer> pack = gameInput.getAssetIds();
        List<String> names = gameInput.getPlayerNames();

        Jucator[] player = new Jucator[names.size()];

        for (int i = 0; i < names.size(); i++) {
            if (names.get(i).equals("basic"))
                player[i] = new Base(names.get(i));
            if (names.get(i).equals("greedy"))
                player[i] = new Greedy(names.get(i));
            if (names.get(i).equals("bribed"))
                player[i] = new Bribe(names.get(i));
        }

        int roundNO = 1;

        // Number of total plays = 2 * number of players (2 times sheriff everyone)
        while (roundNO <= 2 * names.size()) {

            // Picking cards from pack
            for (int i = 0; i < names.size(); i++)
                player[i].takecards(pack);

            // Choosing the sheriff
            // Three players situation
            if (names.size() == 3) {
                if (roundNO == 1 || roundNO == 4)
                    player[0].sheriff = true;
                else if (roundNO == 2 || roundNO == 5)
                    player[1].sheriff = true;
                else
                    player[2].sheriff = true;
            }
            // Two players situation
            else if (names.size() == 2) {
                if (roundNO == 1 || roundNO == 3)
                    player[0].sheriff = true;
                else
                    player[1].sheriff = true;
            }

            // Sheriff checkin'
            if (names.size() == 2) {
                if (player[0].sheriff) {
                    player[1].playnormal(pack, roundNO);
                    player[0].playsheriff(player[1]);
                } else {
                    player[0].playnormal(pack, roundNO);
                    player[1].playsheriff(player[0]);
                }
            } else if (names.size() == 3) {
                if (player[0].sheriff) { // == true
                    player[1].playnormal(pack, roundNO);
                    player[2].playnormal(pack, roundNO);
                    player[0].playsheriff(player[1]);
                    player[0].playsheriff(player[2]);
                } else if (player[1].sheriff) {
                    player[0].playnormal(pack, roundNO);
                    player[2].playnormal(pack, roundNO);
                    player[1].playsheriff(player[0]);
                    player[1].playsheriff(player[2]);
                } else {
                    player[0].playnormal(pack, roundNO);
                    player[1].playnormal(pack, roundNO);
                    player[2].playsheriff(player[0]);
                    player[2].playsheriff(player[1]);
                }
            }

            for (int i = 0; i < names.size(); i++)
                player[i].sheriff = false;

            roundNO++;

        }

        // Add illegal bonuses and calculate profit
        for (int i = 0; i < names.size(); i++) {

            int cheeseTOadd = 0;
            int chickenTOadd = 0;
            int breadTOadd = 0;

            for (int j = 0; j < player[i].AssetsOnMerchantStand.size(); j++) {
                if (player[i].AssetsOnMerchantStand.get(j).type.equals("Silk"))
                    cheeseTOadd += 3; // 3X Cheese bonus
                if (player[i].AssetsOnMerchantStand.get(j).type.equals("Pepper"))
                    chickenTOadd += 2; // 2X Chicken bonus
                if (player[i].AssetsOnMerchantStand.get(j).type.equals("Barrel"))
                    breadTOadd += 2; // 2X Bread bonus
            }
            while(cheeseTOadd != 0) {
                player[i].AssetsOnMerchantStand.add(new Carte("Cheese"));
                cheeseTOadd--;
            }
            while(chickenTOadd != 0) {
                player[i].AssetsOnMerchantStand.add(new Carte("Chicken"));
                chickenTOadd--;
            }
            while(breadTOadd != 0){
                player[i].AssetsOnMerchantStand.add(new Carte("Bread"));
                breadTOadd--;
            }

            // Calculate points until now
            player[i].points = player[i].Coins;
            for (int j = 0; j < player[i].AssetsOnMerchantStand.size(); j++) {
                player[i].points += player[i].AssetsOnMerchantStand.get(j).profit;
            }
        }

        // Setting a evidence of products for every player
        for(int i=0; i<names.size(); i++)
            player[i].getProducts();

        // King/Queen bonuses array
        int[] kqbonuses = new int[8];
        kqbonuses[0] = 20;
        kqbonuses[1] = 10;
        kqbonuses[2] = 15;
        kqbonuses[3] = 10;
        kqbonuses[4] = 15;
        kqbonuses[5] = 10;
        kqbonuses[6] = 10;
        kqbonuses[7] = 5;

        // Adding King/Queen bonus points
        // Two players situation
        if(names.size() == 2) {
            for (int j = 0; j < 4; j++)
                if (player[0].products[j] > player[1].products[j]) {
                    player[0].points += kqbonuses[2 * j];
                    player[1].points += kqbonuses[2 * j + 1];
                } else if (player[0].products[j] < player[1].products[j]) {
                    player[0].points += kqbonuses[2 * j + 1];
                    player[1].points += kqbonuses[2 * j];
                } else if (player[0].products[j] == player[1].products[j]) {
                    player[0].points += kqbonuses[2 * j];
                    player[1].points += kqbonuses[2 * j];
                }
        }
        // Three players situation
        else if(names.size() == 3) {

            // Two indexes to point at other players correctly
            int index1; // =0
            int index2;

            for (int i = 0; i < 3; i++) {

                if (i == 0) {
                    index1 = 1;
                    index2 = 2;
                } else if (i == 1) {
                    index1 = 0;
                    index2 = 2;
                } else {
                    index1 = 0;
                    index2 = 1;
                }
                for (int j = 0; j < 4; j++) {
                    if (player[i].products[j] >= Math.max(player[index1].products[j], player[index2].products[j])) {
                        player[i].points += kqbonuses[2 * j];
                        if (player[index1].products[j] != player[i].products[j] && player[index2].products[j] != player[i].products[j]) {

                            if (player[index1].products[j] > player[index2].products[j]) {
                                player[index1].points += kqbonuses[2 * j + 1];
                            } else if (player[index1].products[j] < player[index2].products[j]) {
                                player[index2].points += kqbonuses[2 * j + 1];
                            } else if (player[index1].products[j] == player[index2].products[j]) {
                                player[index1].points += kqbonuses[2 * j + 1];
                                player[index2].points += kqbonuses[2 * j + 1];
                            }
                        } else {
                            if (player[index1].products[j] == player[i].products[j])
                                player[index1].points += kqbonuses[2 * j];
                            else
                                player[index1].points += kqbonuses[2 * j + 1];

                            if (player[index2].products[j] == player[i].products[j])
                                player[index2].points += kqbonuses[2 * j];
                            else
                                player[index2].points += kqbonuses[2 * j + 1];
                        }
                        kqbonuses[2*j] = 0;
                        kqbonuses[2*j+1] = 0;
                    }
                }
            }
        }

        // Bubblesort players after score descending
        player[0].bubbleSort(player);

        // Printing the final scores
        for(int i=0; i<names.size(); i++)
                System.out.println(player[i].type.toUpperCase() + ": " + player[i].points);

    }
}
